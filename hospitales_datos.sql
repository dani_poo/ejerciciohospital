
INSERT INTO `dept2` VALUES
(10, 'CONTABILIDAD', 'MADRID'),
(20, 'INVESTIGACIÓN', 'BILBAO'),
(30, 'VENTAS', 'SEVILLA'),
(40, 'OPERACIONES', 'MALAGA');

INSERT INTO `emp` VALUES
(7369, 'SANCHEZ', 'EMPLEADO', NULL, '1980-12-17', 104000, 0, 20),
(7499, 'ARROYO', 'VENDEDOR', NULL, '1981-02-20', 208000, 39000, 30),
(7521, 'SALA', 'VENDEDOR', NULL, '1981-02-22', 162500, 65000, 30),
(7566, 'JIMENEZ', 'DIRECTOR', NULL, '1981-04-02', 386750, 0, 20),
(7654, 'ARENAS', 'VENDEDOR', NULL, '1981-09-28', 162500, 182000, 30),
(7698, 'NEGRO', 'DIRECTOR', NULL, '1981-05-01', 370500, 0, 30),
(7782, 'CEREZO', 'DIRECTOR', NULL, '1981-06-09', 318500, 0, 10),
(7788, 'GIL', 'ANALISTA', NULL, '1982-12-09', 390000, 0, 20),
(7839, 'REY', 'PRESIDENTE', NULL, '1981-11-17', 650000, 0, 10),
(7844, 'TOVAR', 'VENDEDOR', NULL, '1981-10-08', 195000, 0, 30),
(7876, 'ALONSO', 'EMPLEADO', NULL, '1983-01-12', 143000, 0, 20),
(7900, 'JIMENO', 'EMPLEADO', NULL, '1981-12-03', 123500, 0, 30),
(7902, 'FERNEANDEZ', 'ANALISTA', NULL, '1981-12-03', 390000, 0, 20),
(7934, 'MUÑOZ', 'EMPLEADO', NULL, '1981-01-23', 169000, 0, 10);


UPDATE emp
SET dir=7902 WHERE emp_no=7369;
UPDATE emp
SET dir=7698 WHERE emp_no=7499;
UPDATE emp
SET dir=7698 WHERE emp_no=7521;
UPDATE emp
SET dir=7839 WHERE emp_no=7566;
UPDATE emp
SET dir=7698 WHERE emp_no=7654;
UPDATE emp
SET dir=7839 WHERE emp_no=7698;
UPDATE emp
SET dir=7839 WHERE emp_no=7782;
UPDATE emp
SET dir=7566 WHERE emp_no=7788;
UPDATE emp
SET dir=7698 WHERE emp_no=7844;
UPDATE emp
SET dir=7788 WHERE emp_no=7876;
UPDATE emp
SET dir=7698 WHERE emp_no=7900;
UPDATE emp
SET dir=7566 WHERE emp_no=7902;
UPDATE emp
SET dir=7782 WHERE emp_no=7934;

INSERT INTO `hospital` VALUES
(13, 'Provincial', 'O Donell 50', '964-4264', 502),
(18, 'General', 'Atocha s/n', '595-3111', 987),
(22, 'La Paz', 'Castellana 1000', '923-5411', 412),
(45, 'San Carlos', 'Ciudad Universitaria', '597-1500', 845);


INSERT INTO `doctor` VALUES
(386, 22, 'Cabeza D.', 'Psiquiatría'),
(398, 22, 'Best K.', 'Urología'),
(435, 13, 'Lopez A.', 'Cardiologia'),
(453, 22, 'Galo D.', 'Pediatría'),
(522, 45, 'Adams C.', 'Neurología'),
(585, 18, 'Miller G.', 'Ginecología'),
(607, 45, 'Nico P.', 'Pediatría'),
(982, 18, 'Cajal R.', 'Cardiología');

INSERT INTO `enfermo` VALUES
(10995, 'Laguia M.', 'Recoletos 50', '1967-06-23', 'M', '280862482'),
(18004, 'Serrano V.', 'Alcala 12', '1960-05-21', 'F', '284991452'),
(14024, 'Fernandez M', 'Recoletos 50', '1967-06-23', 'F', '321790059'),
(36658, 'Domin S.', 'Mayor 71', '1942-01-01', 'M', '160657471'),
(38702, 'Neal R.', 'Orense 11', '1940-06-18', 'F', '380010217'),
(39217, 'Cervantes M.', 'Peron 38', '1952-02-29', 'M', '440294390'),
(59076, 'Miller G', 'Lopez Hoyos 2', '1945-09-16', 'F', '311969044'),
(63827, 'Ruiz P.', 'Esquerdo 103', '1980-12-26', 'M', '100973253'),
(64823, 'Fraser A.', 'Soto 3', '1980-07-10', 'F', '285201776'),
(74835, 'Benitez E.', 'Argentina 5', '1957-10-05', 'M', '154811767');

INSERT INTO `sala` VALUES
(1, 22, 'Recuperación', 10),
(1, 45, 'Recuperación', 13),
(2, 22, 'Maternidad', 34),
(2, 45, 'Maternidad', 24),
(3, 13, 'Cuidados Intensivos', 21),
(3, 18, 'Cuidados Intensivos', 10),
(4, 18, 'Cardiología', 53),
(4, 45, 'cardiología', 55),
(6, 13, 'Psiquiátrico', 67),
(6, 22, 'Psiquiátrico', 118);



INSERT INTO `ocupacion` VALUES
(10995, 13, 3, 1),
(14024, 13, 3, 3),
(18004, 13, 3, 2),
(36658, 18, 4, 1),
(38702, 18, 4, 2),
(39217, 22, 6, 1),
(59076, 22, 6, 2),
(63827, 22, 6, 3),
(64823, 22, 2, 1);

INSERT INTO `plantilla` VALUES
(13, 6, 3106, 'Hernandez J.', 'Enfermero', 'T', 27550000),
(13, 6, 3754, 'Diaz B.', 'Enfermera', 'T', 2262000),
(18, 4, 6357, 'Karplus W.', 'Interno', 'T', 3379000),
(22, 1, 6065, 'Rivera G.', 'Enfermera', 'N', 1626000),
(22, 1, 7379, 'Carlos R.', 'Enfermera', 'T', 2119000),
(22, 2, 1234, 'Garcia J.', 'Enfermo', 'M', 3000000),
(22, 2, 2222, 'Garcia J', '', '', 0),
(22, 2, 9901, 'Nuñez C.', 'Interno', 'M', 2210000),
(22, 6, 1009, 'Higueras D.', 'Enfermera', 'T', 2005000),
(22, 6, 8422, 'Bocina G.', 'Enfermero', 'M', 1638000),
(45, 1, 8526, 'Frank H.', 'Enfermera', 'T', 2522000),
(45, 4, 1280, 'Amigo R.', 'Interno', 'N', 2210000);

