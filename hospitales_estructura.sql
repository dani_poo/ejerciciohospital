
DROP DATABASE IF EXISTS `hospitales`;
CREATE DATABASE `hospitales`
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
 
USE `hospitales`;

CREATE TABLE `hospital` (
  `hospital_cod` int(2) NOT NULL,
  `nombre` varchar(10),
  `direccion` varchar(20),
  `telefono` varchar(8),
  `num_cama` int(3)
);

ALTER TABLE `hospital`
  ADD PRIMARY KEY (`hospital_cod`);

CREATE TABLE `sala` (
  `sala_cod` int(2) NOT NULL,
  `hospital_cod` int(2) NOT NULL,
  `nombre` varchar(20),
  `num_cama` int(3)
);

ALTER TABLE `sala`
  ADD PRIMARY KEY (`sala_cod`,`hospital_cod`),
  ADD KEY `hospital_cod` (`hospital_cod`);

CREATE TABLE `plantilla` (
  `hospital_cod` int(2) NOT NULL,
  `sala_cod` int(2) NOT NULL,
  `empleado_no` int(4) NOT NULL,
  `apellido` varchar(15),
  `funcion` varchar(10),
  `turno` varchar(1),
  `salario` int(10)
);

ALTER TABLE `plantilla`
  ADD PRIMARY KEY (`hospital_cod`,`sala_cod`,`empleado_no`),
  ADD KEY `sala_cod` (`sala_cod`);


CREATE TABLE `ocupacion` (
  `inscripcion` int(5) NOT NULL,
  `hospital_cod` int(2) NOT NULL,
  `sala_cod` int(2) NOT NULL,
  `cama` int(4) NOT NULL
);

ALTER TABLE `ocupacion`
  ADD PRIMARY KEY (`inscripcion`,`hospital_cod`,`sala_cod`),
  ADD KEY `hospital_cod` (`hospital_cod`),
  ADD KEY `sala_cod` (`sala_cod`);

CREATE TABLE `doctor` (
  `doctor_no` int(3) NOT NULL,
  `hospital_cod` int(2) NOT NULL,
  `apellido` varchar(13),
  `especialidad` varchar(16)
);

ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctor_no`,`hospital_cod`),
  ADD KEY `hospital_cod` (`hospital_cod`);

CREATE TABLE `enfermo` (
  `inscripcion` int(5) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `fecha-nac` date NOT NULL,
  `s` varchar(1) NOT NULL,
  `nss` varchar(9) NOT NULL
);

ALTER TABLE `enfermo`
  ADD KEY `inscripcion` (`inscripcion`);



CREATE TABLE `dept2` (
  `dept_no` int(2) NOT NULL,
  `dnombre` varchar(14),
  `loc` varchar(14)
);

ALTER TABLE `dept2`
ADD PRIMARY KEY (`dept_no`);

CREATE TABLE `emp` (
  `emp_no` int(4) NOT NULL,
  `apellido` varchar(10),
  `oficio` varchar(10),
  `dir` int(4),
  `fecha_alta` date,
  `salario` int(10),
  `comision` int(10),
  `dept_no` int(2) NOT NULL
);

ALTER TABLE `emp`
  ADD PRIMARY KEY (`emp_no`),
  ADD KEY `emp_no` (`emp_no`),
  ADD KEY `dept_no` (`dept_no`),
  ADD KEY `dir` (`dir`);

ALTER TABLE `doctor`
  ADD CONSTRAINT `fk_doctor_hospital` FOREIGN KEY (`hospital_cod`) REFERENCES `hospital` (`hospital_cod`);


ALTER TABLE `ocupacion`
  ADD CONSTRAINT `fk_ocupacion_hospital` FOREIGN KEY (`hospital_cod`) REFERENCES `hospital` (`hospital_cod`),
  ADD CONSTRAINT `fk_ocupacion_sala` FOREIGN KEY (`sala_cod`) REFERENCES `sala` (`sala_cod`),
  ADD CONSTRAINT `fk_ocupacion_enfermo` FOREIGN KEY (`inscripcion`) REFERENCES `enfermo` (`inscripcion`);


ALTER TABLE `plantilla`
  ADD CONSTRAINT `fk_plantilla_hospital` FOREIGN KEY (`hospital_cod`) REFERENCES `hospital` (`hospital_cod`),
  ADD CONSTRAINT `fk_plantilla_sala` FOREIGN KEY (`sala_cod`) REFERENCES `sala` (`sala_cod`);


ALTER TABLE `sala`
  ADD CONSTRAINT `fk_sala_hospital` FOREIGN KEY (`hospital_cod`) REFERENCES `hospital` (`hospital_cod`);

ALTER TABLE `emp`
  ADD CONSTRAINT `fk_emp_dept2` FOREIGN KEY (`dept_no`) REFERENCES `dept2` (`dept_no`),
  ADD CONSTRAINT `fk_emp_jefe` FOREIGN KEY (`dir`) REFERENCES `emp` (`emp_no`);