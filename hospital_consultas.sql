-- 1.- Encuentre a todos los miembros del personal cuyo inicial del nombre empiece por 'H'

SELECT DISTINCT * FROM `plantilla` WHERE apellido LIKE '%H.';

-- 2. �Qui�nes son las enfermeras y enfermeros que trabajan en turnos de Tarde o Ma�ana?

SELECT DISTINCT empleado_no, apellido FROM  plantilla
WHERE plantilla.funcion like 'enfermer%' and (turno='T' or turno='M');

-- 3. Haga un listado de las enfermeras que ganan entre 2.000.000 y 2.500.000 Pts.
SELECT DISTINCT empleado_no, apellido FROM plantilla
WHERE funcion = 'enfermera' and salario BETWEEN 2000000 AND 2500000;

-- 4. Mostrar, para todos los hospitales, el c�digo de hospital, el nombre completo del hospital
-- y su nombre abreviado de tres letras (a esto podemos llamarlo ABR) Ordenar la
-- recuperaci�n por esta abreviatura. 

SELECT hospital_cod, nombre, LEFT((REPLACE(nombre,' ','')),3) AS ABR FROM hospital ORDER by 3;

/*
7. Crea una consulta para conseguir la siguiente salida:
COMENTARIO
--------------------
El departamento de CONTABILIDAD esta en SEVILLA
El departamento de INVESTIGACI�N est� en MADRID
El departamento de VENTAS esta en BARCELONA
El departamento de PRODUCCI�N est� en BILBAO
*/

SELECT concat ('El departamento de ',replace(dnombre,'OPERACIONES','PRODUCCI�N'), 
IF (
    loc='MADRID',
    ' esta en SEVILLA'
    , IF (loc='BILBAO',' est� en MADRID', 
          if(loc='sevilla', ' esta en BARCELONA',' est� en BILBAO')
          )
   )

) AS COMENTARIO  FROM dept2;

-- 16. Encontrar el salario medio de los Analistas.
SELECT AVG(salario) as 'salario medio analista' FROM emp WHERE oficio='Analista';

-- 17. Encontrar el salario m�s alto y el salario m�s bajo de la tabla de empleados, as� como la
-- diferencia entre ambos.
SELECT MAX(salario) as 'salario_m�ximo', MIN(salario) as 'salario_m�nimo', MAX(salario)-MIN(salario) 'diferencia'   FROM emp;

-- 18. Calcular el n�mero de oficios diferentes que hay, en total, en los departamentos 10 y 20
-- de la empresa

select t.oficio, count(*)'numero de oficios de los departamentos 10 y 20'  from (select oficio,dept_no from emp WHERE dept_no=10 or dept_no=20)t GROUP by 1;

-- 19. Calcular el n�mero de personas que realizan cada oficio en cada departamento.
SELECT  oficio, dept_no, COUNT(*) 'n�mero de personas' FROM emp GROUP BY 2,1;

-- 20. Buscar que departamentos tienen m�s de cuatro personas trabajando.
select dnombre as departamentos_con_m�s_de_4_trabajadores from emp JOIN dept2 USING(dept_no) group by emp.dept_no HAVING count(*)>4;

-- 21. Buscar que departamentos tienen m�s de dos personas trabajando en la misma
-- profesi�n.
select  dnombre as 'departamentos con m�s de dos personas trabajando en la misma profesi�n' from emp JOIN dept2 USING(dept_no) GROUP BY emp.dept_no, oficio HAVING COUNT(*)>2;

-- 22. Se desea saber el n�mero de empleados por departamento que tienen por oficio el de
-- "EMPLEADO". La salida debe estar ordenada por el numero de departamento.
select dept_no as 'n�mero de departamento', count(*)'n�mero de empleados de profesi�n oficio' from emp where oficio='empleado' GROUP by dept_no ORDER by 1 ASC;

-- 23. Se desea saber el salario total (salario m�s comisi�n) medio anual de los vendedores de
-- nuestra empresa.

SELECT (sum(salario)+sum(comision))/count(*) as 'salario medio anual, contando comisiones' from emp;

-- 24. Se desea saber el salario total (salario m�s comisi�n) medio anual, tanto de los
-- empleados como de los vendedores de nuestra empresa.

SELECT oficio,(sum(salario)+sum(comision))/count(*) as 'salario medio' from emp GROUP by  1 HAVING oficio='empleado' or oficio='vendedor';

/* 25. Se desea saber para cada departamento y en cada oficio, el m�ximo salario y la suma
 total de salarios, pero solo de aquellos departamentos y oficios cuya suma salarial supere o
sea igual que el 50% de su m�ximo salario. En el muestreo, solo se estudiaron a aquellos
empleados que no tienen comisi�n o la tengan menor que el 25% de su salario. */

SELECT dept_no, oficio, MAX(salario) 'salario maximo' FROM 
(SELECT * FROM EMP WHERE comision<>0 and (SALARIO*0.25)<=comision) as muestreo
GROUP BY 1, 2 having sum(salario)>=MAX(salario)*0.5;

/* 30.- Queremos saber el m�ximo, el m�nimo y la media salarial, de cada departamento de la
empresa. */
SELECT dnombre 'Departamento', MAX(salario)'salario max', MIN(salario) 'salario min', AVG(salario)'salario med' FROM emp join dept2 USING(dept_no) GROUP BY 1

-- 31. Listar, a partir de las tablas EMP y DEPT2, el nombre de cada empleado, su
-- oficio, su n�mero de departamento y el nombre del departamento donde trabajan.
SELECT apellido, oficio, emp.dept_no, dnombre from dept2 JOIN emp USING (dept_no);

-- 32. Seleccionar los nombres, profesiones y localidades de los departamentos 
-- donde trabajan los Analistas.
-- select DISTINCT dept_no from emp where oficio='analista'
SELECT dnombre, oficio, loc from dept2 INNER JOIN emp USING (dept_no) where dept_no in (select DISTINCT dept_no from emp where oficio='analista');

-- 33. Se desea conocer el nombre y oficio de todos aquellos empleados 
-- que trabajan en Madrid. La salida deber� estar ordenada por el oficio.
SELECT apellido, oficio from emp INNER JOIN dept2 USING (dept_no) WHERE loc='madrid' ORDER BY 2;

-- 34. se desea conocer cu�ntos empleados existen en cada departamento. Devolviendo una
-- salida como la que se presenta (deber� estar ordenada por el n�mero de empleados descendentemente).

SELECT dept_no, dnombre, COUNT(*) 'n�mero de empleados' FROM emp join dept2 USING(dept_no) GROUP BY dept_no;

-- 35. Se desea conocer, tanto para el departamento de VENTAS, como para el de
-- CONTABILIDAD, su m�ximo, su m�nimo y su media salarial, as� como el n�mero de
-- empleados en cada departamento. La salida deber� estar ordenada por el nombre del
-- departamento, y se deber� presentar como la siguiente:
--    DNOMBRE MAXIMO M�NIMO MEDIA N-EMPL
-- ----------- ------ ----    ----   ----
-- CONTABILIDAD 650000 169000 379166.667 3
-- VENTAS 370500 123500 203666.667 6
SELECT dnombre DNOMBRE, max(salario) MAXIMO, min(salario) M�NIMO, avg(salario) MEDIA, COUNT(*) 'N-EMPL' from emp INNER join dept2 USING(dept_no) GROUP BY dept_no HAVING dnombre='ventas' OR dnombre='contabilidad' ORDER by 1;

-- 37. Se desea obtener un resultado como el que aparece, en el que se presenta el numero,
-- nombre y oficio de cada empleado de nuestra empresa que tiene jefe, y lo mismo de su jefe
-- directo. La salida debe estar ordenada por el nombre del empleado.

SELECT  emp.emp_no EMPLEADO, emp.apellido NOMBRE, emp.oficio OFICIO,
        jefe.emp_no JEFE, jefe.apellido NOMBRE, jefe.oficio OFICIO
FROM emp INNER JOIN emp AS jefe ON (emp.dir = jefe.emp_no)
ORDER BY 1;

-- 38. Se desea conocer, para todos los departamentos existentes, el m�nimo salario de cada
-- departamento, mostrando el resultado como aparece. Para el muestreo del m�nimo salario,
-- no queremos tener en cuenta a las personas con oficio de EMPLEADO. La salida estar�
-- ordenada por el salario descendentemente.
SELECT dnombre 'DEPARTAMENTO', min(salario) 'M�NIMO' from dept2 natural join emp where oficio!='empleado' GROUP by dept_no;

-- 42. Obtener el apellido, departamento y oficio de aquellos empleados que
-- tengan un oficio que este en el departamento 20 y que no sea ninguno de los -- oficios que esta en el departamento de VENTAS.

 select apellido, dnombre, oficio
 from dept2 inner join emp on dept2.dept_no=emp.dept_no and dnombre<>'VENTAS'
 where oficio in (SELECT distinct oficio from emp where dept_no=20)
 
-- 43. Obtener el n�mero de empleado, numero de departamento y apellido de
-- todos los empleados que trabajen en el departamento 20 o 30 y su salario
-- sea mayor que dos veces el m�nimo de la empresa. No queremos que el oficio sea PRESIDENTE.

SELECT emp_no, dept_no, apellido
FROM emp
WHERE (dept_no=20 or dept_no=30)
and salario>(SELECT min(salario)*2 FROM emp)
and oficio<>'presidente';

				
-- 44. Encontrar las personas que ganan 500.000 PTA m�s que el miembro del
-- personal de sueldo m�s alto del turno de ma�ana y que tenga el mismo
-- trabajo que el Sr. Nu�ez.
select *
from plantilla
where salario>
            (select salario+500000
            from plantilla
            where salario= (SELECT max(salario)
                            FROM  plantilla
                            where turno='M'))
and funcion in 
            (SELECT distinct funcion
            from plantilla
            where apellido like 'nu�ez%');

-- 45. Queremos averiguar el apellido del individuo m�s antiguo de la empresa.
SELECT DISTINCT apellido
FROM emp
where fecha_alta = 
				(select min(fecha_alta)
                 FROM emp);

-- 46. Presentar los nombres y oficios de los empleados que tienen
-- el mismo trabajo que JIMENEZ.
select apellido, oficio
from emp
where oficio in (select distinct oficio from emp
    			where apellido like 'jimenez%')
AND
apellido NOT LIKE '%jimenez%';

-- 47. Queremos conocer el apellido, oficio, salario y departamento en
-- el que trabajan, de todos los individuos cuyo salario sea mayor que 
-- el mayor salario del departamento 30.

SELECT apellido, oficio, salario, dept_no
from emp
where salario > 
				(SELECT max(salario)
                 FROM emp
                 where dept_no='30');

-- 48. Presentar los nombres y oficios de todos los empleados del 
-- departamento 20, cuyo trabajo sea id�ntico al de cualquiera de los
-- empleados del departamento de VENTAS.
SELECT DISTINCT apellido, oficio
from emp
where oficio in (
            SELECT distinct oficio 
            FROM (SELECT DISTINCT oficio 
                  FROM  emp
                  WHERE dept_no=20) as c1
            INNER JOIN
                (SELECT DISTINCT oficio 
                 FROM dept2 NATURAL JOIN emp
                 WHERE dnombre='VENTAS') as c2
            USING (oficio));

-- 49. Se desea obtener todos los empleados de los departamentos que no
-- ganan ni el m�ximo ni el m�nimo salarial de la empresa.
SELECT *
FROM emp
WHERE salario not IN (
    				  (SELECT MAX(salario)
    				  FROM emp),
						(SELECT MIN(salario)
    				  FROM emp));

-- 53. En qu� departamento se dio de alta a m�s empleados en diciembre.

select dept_no 'departamento con m�s altas de empleados en diciembre'
from
	(SELECT dept_no, COUNT(*) n
    from emp
    WHERE MONTH (fecha_alta) =12
    GROUP by 1) c2
WHERE
n = (select max(n)
		from (
    	SELECT dept_no, COUNT(*) n
    	from emp
    	WHERE MONTH (fecha_alta) =12
    	GROUP by 1) c1);

-- 55. Queremos saber el nombre del empleado m�s joven de cada departamento, 
-- as� como el nombre de este.

select  emp.dept_no, apellido, emp.fecha_alta
from emp 
	INNER JOIN
	(SELECT dept_no, max(fecha_alta) fecha_alta
	from emp
	GROUP by dept_no) c1
    ON (emp.fecha_alta=c1.fecha_alta and emp.dept_no=c1.dept_no);

-- 56. Se desea saber el nombre, oficio y departamento del empleado 
-- que m�s gana del departamento con la media salarial m�s alta.
select *, max(salario)
from emp
where dept_no in (

select dept_no
from (
    SELECT  dept_no, MAX(salario)
    from (
        SELECT dept_no, avg(salario) salario
        FROM emp
        GROUP by 1) c1
						)c2)
GROUP by dept_no;

-- 57. Se desea obtener informaci�n sobre todos los empleados que son 
-- jefes de alguien.
SELECT DISTINCT jefe.emp_no ID_JEFE, jefe.apellido ,jefe.oficio ,jefe.fecha_alta, jefe.salario , jefe.comision, jefe.dept_no
FROM emp JOIN emp AS jefe ON emp.dir=jefe.emp_no

-- 58. Recuperar el numero (empleado_no) y nombre de las personas que perciban un salario >
-- que la media de su hospital.
SELECT empleado_no, apellido
FROM plantilla
WHERE salario > (SELECT AVG (salario)
				FROM plantilla);
				
-- 63. Cambiar al paciente (tabla ENFERMO) n�mero 74835 la direcci�n a Alcal� 411.
UPDATE enfermo
SET direccion='Alcal� 411'
WHERE inscripcion=74835;

-- 64. Poner todas las direcciones de la tabla ENFERMO a null.
UPDATE enfermo
SET direccion=null;

-- 65. Igualar la direcci�n y fecha de nacimiento del paciente 10995
-- a los valores de las columnas correspondientes almacenadas para el 
-- paciente 14024.

UPDATE enfermo
SET apellido=(SELECT apellido FROM enfermo where inscripcion=74835),
    direccion=(SELECT direccion FROM enfermo where inscripcion=74835),
    `fecha-nac`=(SELECT `fecha-nac` FROM enfermo where inscripcion=74835),
    s=(SELECT s FROM enfermo where inscripcion=74835),
    nss=(SELECT nss FROM enfermo where inscripcion=74835)
WHERE inscripcion=10995;
-- forma de insertar una fecha UPDATE tabla SET `fecha-nac`='1967-06-23' WHERE .....

-- 66. En todos los hospitales del pa�s se ha recibido un aumento del
-- presupuesto, por lo que se incrementar� el n�mero de camas disponibles
-- en un 10%. �Como se har�a en SQL?
UPDATE hospital
SET `num_cama`=`num_cama`*0.10+`num_cama`;

-- 69. Crear una vista para los departamentos 10 y 20.
-- - Crear una vista para los departamentos 10 y 30.
-- - Hacer una JOIN de las dos vistas anteriores.
CREATE OR REPLACE VIEW t1 
AS 
SELECT *
FROM dept2
WHERE dept_no=10 OR dept_no=20;

CREATE OR REPLACE VIEW t2
AS 
SELECT *
FROM dept2
WHERE dept_no=20 OR dept_no=23;

SELECT *
FROM  t1 join t2
USING (dept_no);